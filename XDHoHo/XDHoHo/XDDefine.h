//
//  XDDefine.h
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#ifndef XDUI_XDDefine_h
#define XDUI_XDDefine_h

typedef enum {
    XDAccountGenderNone       = -1,
    XDAccountGenderUnknown,
    XDAccountGenderMale,
    XDAccountGenderFemale
} XDAccountGender;

typedef enum {
    XDMediaTypeText       = 0,    //文本
    XDMediaTypeImage      = 1,    //静态图片
    XDMediaTypeGIF        = 2,    //动态图片
    XDMediaTypeAudio      = 3,    //语音
    XDMediaTypeVideo      = 4,    //视频
    XDMediaTypeURI        = 5,    //链接
    XDMediaTypeLocation   = 6,    //位置
    XDMediaTypeMood       = 99    //心情
} XDMediaType;

typedef enum {
    XDAddressStyleText       = 0,
    XDAddressStyleMap,
} XDAddressStyle;

//发送者信息
#define KMODEL_ID @"ID"//事件id
#define KMODEL_ACCOUNTID @"accountId"//用户id
#define KMODEL_NAME @"name"//姓名
#define KMODEL_HEADPATH @"headImagePath"//头像路径
#define KMODEL_GENDER @"gender"//性别
#define KMODEL_CREATEDATE @"createDate"//创建时间
#define KMODEL_ADDRESS @"address"//地址
#define KMODEL_LATITUDE @"latitude"//经度
#define KMODEL_LONGITUDE @"longitude"//纬度
//正文
#define KMODEL_TITLE @"title"//标题
#define KMODEL_SUBTITLE @"subtitle"//副标题
#define KMODEL_CONTENT @"content"//内容
#define KMODEL_REMARK @"remark"//备注

//动态特有
#define KACTIVITY_COUNT_COMMENT @"commentCount"
#define KACTIVITY_COUNT_PRAISE @"praiseCount"
#define KACTIVITY_COUNT_TRAMPLE @"trampleCount"
#define KACTIVITY_COUNT_FAVORITE @"favoriteCount"

//附件特有
#define KAPPENDIX @"appendix"
#define KAPPENDIX_IMAGE @"appendixImage"
#define KAPPENDIX_VIDEO @"appendixVideo"
#define KAPPENDIX_AUDIO @"appendixAudio"

#define KAPPENDIX_SOURCEPATH @"appendixSourcePath" //附件源文件路径
#define KAPPENDIX_DURATION @"appendixDuration" //时长(秒)。音频、视频总时长
#define KAPPENDIX_SIZE @"appendixSize" //大小(字节)。音频、视频文件大小
#define KAPPENDIX_RELATED @"appendixRelated"//关联对象

#define THUMB_IMAGE_SIZE    CGSizeMake(100.0, 100.0)    //小图大小
#define BIG_IMAGE_SIZE      CGSizeMake(270.0, 200.0)    //大图大小
#define MOOD_IMAGE_SIZE     CGSizeMake(120.0, 60.0)     //心情图片大小
#define AUDIO_IMAGE_SIZE    CGSizeMake(120.0, 60.0)     //语音图片大小

//详情页特有

//评论特有

#endif
