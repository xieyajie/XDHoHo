//
//  XDAccountViewController.m
//  XDHoHo
//
//  Created by xie yajie on 13-11-16.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "XDAccountViewController.h"

#import "XDLoginViewController.h"

@interface XDAccountViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    //测试变量
    BOOL _isLogin;
}

@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UITableView *tableView;

@property (strong, nonatomic) UIView *tableHeadView;
@property (strong, nonatomic) UIImageView *headView;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UIButton *stateButton;

@end

@implementation XDAccountViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        _isLogin = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.view addSubview:self.topView];
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (UIView *)topView
{
    if (_topView == nil) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        _topView.backgroundColor = [UIColor whiteColor];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _topView.frame.size.width, _topView.frame.size.height)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.text = @"我的";
//        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [_topView addSubview:titleLabel];
    }
    
    return _topView;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.topView.frame.origin.y + self.topView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - (self.topView.frame.origin.y + self.topView.frame.size.height) - 44) style:UITableViewStylePlain];
        _tableView.scrollEnabled = NO;
            _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _tableView.delegate = self;
//        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
        _tableView.tableHeaderView = self.tableHeadView;
    }
    
    return _tableView;
}

- (UIView *)tableHeadView
{
    if (_tableHeadView == nil) {
        _tableHeadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 80)];
        _tableHeadView.backgroundColor = [UIColor orangeColor];
        
        [_tableHeadView addSubview:self.headView];
        [_tableHeadView addSubview:self.nameLabel];
        [_tableHeadView addSubview:self.stateButton];
        [self refreshTableHeadView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTableHeadView:)];
        [_tableHeadView addGestureRecognizer:tap];
    }
    
    return _tableHeadView;
}

- (UIImageView *)headView
{
    if (_headView == nil) {
        _headView = [[UIImageView alloc] initWithFrame:CGRectMake(20, (self.tableHeadView.frame.size.height - 50) / 2, 50, 50)];
        _headView.layer.borderWidth = 1.0;
        _headView.layer.borderColor = [[UIColor colorWithWhite:0.8 alpha:1.0] CGColor];
        _headView.layer.cornerRadius = 25;
    }
    
    return _headView;
}

- (UILabel *)nameLabel
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.backgroundColor = [UIColor clearColor];
    }
    
    return _nameLabel;
}

- (UIButton *)stateButton
{
    if (_stateButton == nil) {
        _stateButton = [[UIButton alloc] init];
        _stateButton.enabled = NO;
    }
    
    return _stateButton;
}

#pragma mark - GestureRecognizer

- (void)tapTableHeadView:(UITapGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateEnded) {
        //操作账号（登陆、注销）
        if (!_isLogin) {
            XDLoginViewController *loginViewController = [[XDLoginViewController alloc] initWithStyle:UITableViewStylePlain];
            [self.tabBarController presentViewController:[[UINavigationController alloc] initWithRootViewController:loginViewController] animated:YES completion:nil];
        }
        else{
            
        }
        //测试
        _isLogin = !_isLogin;
        [self refreshTableHeadView];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AccountCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    // ...
    // Pass the selected object to the new view controller.
}


#pragma mark - private

- (void)refreshTableHeadView
{
    if (_isLogin) {
        self.nameLabel.frame = CGRectMake(80, (self.tableHeadView.frame.size.height - 20) / 2, 100, 20);
        self.nameLabel.font = [UIFont systemFontOfSize:18.0];
        self.nameLabel.textColor = [UIColor whiteColor];
        self.nameLabel.text = @"xieyajie";
        
        self.stateButton.frame = CGRectMake(self.tableHeadView.frame.size.width - 50, (self.tableHeadView.frame.size.height - 30) / 2, 30, 30);
        [self.stateButton setTitle:@"" forState:UIControlStateNormal];
        [self.stateButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self.stateButton.layer.cornerRadius = 15.0;
        self.stateButton.backgroundColor = [UIColor blueColor];
    }
    else{
        self.nameLabel.frame = CGRectMake(80, (self.tableHeadView.frame.size.height - 20) / 2, 100, 20);
        self.nameLabel.font = [UIFont systemFontOfSize:15.0];
        self.nameLabel.textColor = [UIColor grayColor];
        self.nameLabel.text = @"未登录";
        
        self.stateButton.frame = CGRectMake(self.tableHeadView.frame.size.width - 70, (self.tableHeadView.frame.size.height - 20) / 2, 50, 20);
        [self.stateButton setTitle:@"立即登录" forState:UIControlStateNormal];
        [self.stateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.stateButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        self.stateButton.layer.cornerRadius = 5.0;
        self.stateButton.backgroundColor = [UIColor blueColor];
    }
}


@end
