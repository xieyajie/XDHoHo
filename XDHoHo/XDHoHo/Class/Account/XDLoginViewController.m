//
//  XDLoginViewController.m
//  XDHoHo
//
//  Created by xie yajie on 13-11-16.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDLoginViewController.h"

#import "XDGridView.h"

#define COLUMN_COUNT 4

@interface XDLoginViewController ()<XDGridViewDataSource, XDGridViewDelegate>
{
    NSMutableArray *_dataSource;
}

@property (strong, nonatomic) XDGridView *gridView;

@end

@implementation XDLoginViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.title = @"登录";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancleAction:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"menu_login" ofType:@"plist"];
    _dataSource = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (XDGridView *)gridView
{
    if (_gridView == nil) {
        _gridView = [[XDGridView alloc] init];
        _gridView.dataSource = self;
        _gridView.delegate = self;
        _gridView.enableSinglePress = YES;
        _gridView.frame = CGRectMake(12.0, 16.0, self.tableView.frame.size.width - 24.0, ceilf((_dataSource.count + 1.0) / COLUMN_COUNT) * 94.0 + 24.0);
    }
    
    return _gridView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else{
        [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    [cell.contentView addSubview:self.gridView];
    [self.gridView reloadData];
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ceilf((_dataSource.count + 1.0) / COLUMN_COUNT) * 94.0 + 40.0;
}

#pragma mark - XDGridView Delegate

- (NSUInteger)numberOfItemsInGridView:(XDGridView *)gridView
{
    return [_dataSource count];
}

- (NSInteger)gridView:(XDGridView *)gridView numberOfRowsInPage:(NSInteger)page
{
    return ceilf((float)gridView.numberOfItems / (float)COLUMN_COUNT);
}

- (NSInteger)gridView:(XDGridView *)gridView numberOfColumnsInRow:(NSInteger)row page:(NSInteger)page
{
    return COLUMN_COUNT;
}

- (void)gridView:(XDGridView *)gridView setupItem:(XDGridViewItem *)item
{
    NSDictionary *itemInfo = [_dataSource objectAtIndex:item.index];
    
//    item.layer.borderWidth = 1.0;
//    item.layer.borderColor = [[UIColor colorWithWhite:0.8 alpha:0.8] CGColor];
    
    item.textLabel.textColor = [UIColor lightGrayColor];
    item.textLabel.font = [UIFont boldSystemFontOfSize:13.0];
    item.textLabel.text = [itemInfo objectForKey:@"title"];
    
    item.imageView.image = [UIImage imageNamed:[itemInfo objectForKey:@"icon"]];
}

#pragma mark - XDGridViewDelegate

- (void)gridView:(XDGridView *)gridView didSelectItem:(XDGridViewItem *)item
{
    NSLog(item.textLabel.text);
}

#pragma mark - button/item action

- (void)cancleAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
