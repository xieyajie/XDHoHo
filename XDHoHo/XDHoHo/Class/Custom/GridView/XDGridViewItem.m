//
//  XDGridViewItem.m
//  XDHoHo
//
//  Created by xie yajie on 13-11-16.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDGridViewItem.h"

@interface XDGridViewItem ()
{
    UIImageView *_backgroundImageView;
}
@end

@implementation XDGridViewItem

- (id)initWithFrame:(CGRect)frame style:(XDGridViewItemStyle)style
{
    self = [super initWithFrame:frame];
    if (self) {
        _contentView = [[UIView alloc] initWithFrame:self.bounds];
        _contentView.backgroundColor = [UIColor clearColor];
        [self addSubview:_contentView];
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_contentView addSubview:_imageView];

        _textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.font = [UIFont boldSystemFontOfSize:12.0];
        [_contentView addSubview:_textLabel];
        
        _style = style;
        
        _verticalSeparator = 5.0;
        
        [self layoutSubviews];//需直接调用布局方法，预设子视图
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame style:XDGridViewItemStyleDefault];
}

- (id)initWithStyle:(XDGridViewItemStyle)style
{
    return [self initWithFrame:CGRectZero style:style];
}



#pragma mark - Layout

- (void)layoutSubviews
{
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self insertSubview:_backgroundImageView atIndex:0];
    }
    _backgroundImageView.frame = self.bounds;
    _backgroundImageView.image = _isSelected ? _selectedBackgroundImage : _backgroundImage;
    
    _contentView.frame = self.bounds;
    
    if (_style == XDGridViewItemStyleDefault) {
        NSInteger boundLength = MIN(_contentView.frame.size.width - 10.0, _contentView.frame.size.height - 20.0 - _verticalSeparator * 3);//-5-20-10
        _imageView.frame = CGRectMake(0.0, 0.0, boundLength, boundLength);
        _imageView.center = CGPointMake(_contentView.frame.size.width / 2.0, boundLength / 2.0 + _verticalSeparator);
        
        _textLabel.frame = CGRectMake(0.0, _imageView.frame.origin.y + _imageView.frame.size.height + _verticalSeparator, _contentView.frame.size.width, 20.0);
        _textLabel.textAlignment = NSTextAlignmentCenter;
    } else if (_style == XDGridViewItemStyleFillImage) {
        _imageView.frame = _contentView.bounds;
    }
}



#pragma mark - Property

- (void)setVerticalSeparator:(NSUInteger)verticalSeparator
{
    _verticalSeparator = verticalSeparator;
    [self setNeedsLayout];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    _backgroundImage = backgroundImage;
    [self setNeedsLayout];
}

- (void)setSelectedBackgroundImage:(UIImage *)selectedBackgroundImage
{
    _selectedBackgroundImage = selectedBackgroundImage;
    [self setNeedsLayout];
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    [self setNeedsLayout];
}

@end
