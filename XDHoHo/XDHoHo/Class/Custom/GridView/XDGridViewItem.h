//
//  XDGridViewItem.h
//  XDHoHo
//
//  Created by xie yajie on 13-11-16.
//  Copyright (c) 2013年 XD. All rights reserved.
//

typedef enum {
    XDGridViewItemStyleDefault,
    XDGridViewItemStyleTextInside,
    XDGridViewItemStyleFillImage
} XDGridViewItemStyle;

#import <UIKit/UIView.h>

@interface XDGridViewItem : UIView

@property (nonatomic) XDGridViewItemStyle style;

@property (nonatomic) NSUInteger index;
@property (nonatomic) NSUInteger column;//列
@property (nonatomic) NSUInteger row;//行
@property (nonatomic) NSUInteger page;//页

@property (nonatomic) BOOL isSelected;//当前被选中

@property (nonatomic, strong) UIImage *backgroundImage;//背景图片
@property (nonatomic, strong) UIImage *selectedBackgroundImage;//选中背景图片

@property (nonatomic, strong, readonly) UIView *contentView;//容器视图
@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong, readonly) UILabel *textLabel;

@property (nonatomic) NSUInteger verticalSeparator;//垂直方向间隔高度，默认为5.0

- (id)initWithFrame:(CGRect)frame style:(XDGridViewItemStyle)style;
- (id)initWithStyle:(XDGridViewItemStyle)style;

@end
