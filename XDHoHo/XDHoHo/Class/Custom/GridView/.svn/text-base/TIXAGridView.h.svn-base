//
//  TIXAGridView.h
//  Lianxi
//
//  Created by Liusx on 12-11-4.
//  Copyright (c) 2012年 TIXA. All rights reserved.
//

#import <UIKit/UIScrollView.h>
#import "TIXAGridViewItem.h"

typedef enum {
    TIXAGridViewItemAnimationNone,
    TIXAGridViewItemAnimationFade
} TIXAGridViewItemAnimation;

@protocol TIXAGridViewDataSource, TIXAGridViewDelegate;

@interface TIXAGridView : UIView <UIScrollViewDelegate>

@property (nonatomic, weak) id<TIXAGridViewDataSource> dataSource;
@property (nonatomic, weak) id<TIXAGridViewDelegate> delegate;

@property (nonatomic, strong, readonly) UIScrollView *scrollView;//主体scrollView
@property (nonatomic, strong, readonly) UIPageControl *pageControl;//与scrollView协作的页签控制器

@property (nonatomic, readonly) NSUInteger numberOfItems;//item个数
@property (nonatomic, readonly) NSUInteger numberOfColumns;//列数
@property (nonatomic, readonly) NSUInteger numberOfRows;//行数
@property (nonatomic, readonly) NSUInteger numberOfPages;//页数

@property (nonatomic, readonly) CGFloat widthOfItem;//item视图宽度
@property (nonatomic, readonly) CGFloat heightOfItem;//item视图高度

@property (nonatomic) CGFloat borderWidth;//左右边界及间隔，默认5.0
@property (nonatomic) CGFloat borderHeight;//上下边界及间隔，默认5.0

@property (nonatomic, strong, readonly) TIXAGridViewItem *selectedItem;//当前选中item
@property (nonatomic) NSInteger selectedIndex;//当前选中item的index

@property (nonatomic) TIXAGridViewItemStyle itemStlye;//布局样式

@property (nonatomic) BOOL enableLongPress;//允许长按监听
@property (nonatomic) BOOL enableSinglePress;//允许单击监听
@property (nonatomic) BOOL enableDoubleTap;//允许双击监听

- (NSArray *)visibleViews;//当前可见item的视图数组
- (NSArray *)visibleIndexs;//当前可见item的index数组

- (TIXAGridViewItem *)itemAtIndex:(NSUInteger)index;//获取指定item

- (void)reloadData;//重绘全部item
- (void)reloadItemsAtIndexs:(NSArray *)indexs withItemAnimation:(TIXAGridViewItemAnimation)animation;//重绘指定item的视图

- (void)scrollToItemAtIndex:(NSUInteger)index animated:(BOOL)animated;//滚动至指定item

@end



@protocol TIXAGridViewDataSource <NSObject>

- (NSUInteger)numberOfItemsInGridView:(TIXAGridView *)gridView;
//- (NSUInteger)numberOfPagesInGridView:(TIXAGridView *)gridView;
//- (UIView *)gridView:(TIXAGridView *)gridView viewForColumn:(NSUInteger)column row:(NSUInteger)row page:(NSUInteger)page;
@optional
- (NSInteger)gridView:(TIXAGridView *)gridView numberOfRowsInPage:(NSInteger)page;
- (NSInteger)gridView:(TIXAGridView *)gridView numberOfColumnsInRow:(NSInteger)row page:(NSInteger)page;
- (TIXAGridViewItem *)gridView:(TIXAGridView *)gridView itemAtIndex:(NSUInteger)index;
- (void)gridView:(TIXAGridView *)gridView setupItem:(TIXAGridViewItem *)item;

@end



@protocol TIXAGridViewDelegate <NSObject>

@optional
- (void)gridView:(TIXAGridView *)gridView didSelectItem:(TIXAGridViewItem *)item;
- (void)gridView:(TIXAGridView *)gridView didSingleTapItem:(TIXAGridViewItem *)item;
- (void)gridView:(TIXAGridView *)gridView didDoubleTapItem:(TIXAGridViewItem *)item;
- (void)gridView:(TIXAGridView *)gridView didBeginLongPressItem:(TIXAGridViewItem *)item;
- (void)gridView:(TIXAGridView *)gridView didEndLongPressItem:(TIXAGridViewItem *)item;

@end
