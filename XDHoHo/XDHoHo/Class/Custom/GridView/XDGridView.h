//
//  XDGridView.h
//  XDHoHo
//
//  Created by xie yajie on 13-11-16.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIScrollView.h>
#import "XDGridViewItem.h"

typedef enum {
    XDGridViewItemAnimationNone,
    XDGridViewItemAnimationFade
} XDGridViewItemAnimation;

@protocol XDGridViewDataSource, XDGridViewDelegate;

@interface XDGridView : UIView <UIScrollViewDelegate>

@property (nonatomic, weak) id<XDGridViewDataSource> dataSource;
@property (nonatomic, weak) id<XDGridViewDelegate> delegate;

@property (nonatomic, strong, readonly) UIScrollView *scrollView;//主体scrollView
@property (nonatomic, strong, readonly) UIPageControl *pageControl;//与scrollView协作的页签控制器

@property (nonatomic, readonly) NSUInteger numberOfItems;//item个数
@property (nonatomic, readonly) NSUInteger numberOfColumns;//列数
@property (nonatomic, readonly) NSUInteger numberOfRows;//行数
@property (nonatomic, readonly) NSUInteger numberOfPages;//页数

@property (nonatomic, readonly) CGFloat widthOfItem;//item视图宽度
@property (nonatomic, readonly) CGFloat heightOfItem;//item视图高度

@property (nonatomic) CGFloat borderWidth;//左右边界及间隔，默认5.0
@property (nonatomic) CGFloat borderHeight;//上下边界及间隔，默认5.0

@property (nonatomic, strong, readonly) XDGridViewItem *selectedItem;//当前选中item
@property (nonatomic) NSInteger selectedIndex;//当前选中item的index

@property (nonatomic) XDGridViewItemStyle itemStlye;//布局样式

@property (nonatomic) BOOL enableLongPress;//允许长按监听
@property (nonatomic) BOOL enableSinglePress;//允许单击监听
@property (nonatomic) BOOL enableDoubleTap;//允许双击监听

- (NSArray *)visibleViews;//当前可见item的视图数组
- (NSArray *)visibleIndexs;//当前可见item的index数组

- (XDGridViewItem *)itemAtIndex:(NSUInteger)index;//获取指定item

- (void)reloadData;//重绘全部item
- (void)reloadItemsAtIndexs:(NSArray *)indexs withItemAnimation:(XDGridViewItemAnimation)animation;//重绘指定item的视图

- (void)scrollToItemAtIndex:(NSUInteger)index animated:(BOOL)animated;//滚动至指定item

@end



@protocol XDGridViewDataSource <NSObject>

- (NSUInteger)numberOfItemsInGridView:(XDGridView *)gridView;
//- (NSUInteger)numberOfPagesInGridView:(XDGridView *)gridView;
//- (UIView *)gridView:(XDGridView *)gridView viewForColumn:(NSUInteger)column row:(NSUInteger)row page:(NSUInteger)page;
@optional
- (NSInteger)gridView:(XDGridView *)gridView numberOfRowsInPage:(NSInteger)page;
- (NSInteger)gridView:(XDGridView *)gridView numberOfColumnsInRow:(NSInteger)row page:(NSInteger)page;
- (XDGridViewItem *)gridView:(XDGridView *)gridView itemAtIndex:(NSUInteger)index;
- (void)gridView:(XDGridView *)gridView setupItem:(XDGridViewItem *)item;

@end



@protocol XDGridViewDelegate <NSObject>

@optional
- (void)gridView:(XDGridView *)gridView didSelectItem:(XDGridViewItem *)item;
- (void)gridView:(XDGridView *)gridView didSingleTapItem:(XDGridViewItem *)item;
- (void)gridView:(XDGridView *)gridView didDoubleTapItem:(XDGridViewItem *)item;
- (void)gridView:(XDGridView *)gridView didBeginLongPressItem:(XDGridViewItem *)item;
- (void)gridView:(XDGridView *)gridView didEndLongPressItem:(XDGridViewItem *)item;

@end
