//
//  XDSegmentedView.h
//  XDHoHo
//
//  Created by xie yajie on 13-11-12.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    XDSegmentedViewStyleCenterMove = 0,
    XDSegmentedViewStyleTopMove,
    XDSegmentedViewStyleBottomMove
} XDSegmentedViewStyle;

@protocol XDSegmentedViewDelegate;

@interface XDSegmentedView : UIView
{
}

@property (nonatomic , strong) NSArray *buttonTitleDataSource;//标题 -数据源
@property (nonatomic , strong) NSArray *buttonNormalImageDataSource;//一般图片 -数据源
@property (nonatomic , strong) NSArray *buttonSelectedImageDataSource;//选中图片 -数据源
@property (nonatomic , strong) UIColor *buttonTitleNormalColor;//字体一般 -颜色
@property (nonatomic , strong) UIColor *buttonTitleSelectedColor;//字体选中 -颜色
@property (nonatomic , strong) NSArray *badgeValueDataSource;//新消息数 -数据源
@property (nonatomic , assign) id <XDSegmentedViewDelegate> delegate;

@property (nonatomic , assign) NSInteger selectedIndex;//选择的项
@property (nonatomic , strong) NSString *backgroundImgName;//背景图片
@property (nonatomic , strong) NSString *moveImgName;//移动视图
@property (nonatomic , assign) XDSegmentedViewStyle style;//类型
@property (nonatomic , strong) UIFont *font;//字体

@property (nonatomic, strong) NSMutableArray *subButtonList;

@property (nonatomic) BOOL havaDoubleClick; //默认 no

- (void)addTarget:(id)target action:(SEL)action;
- (void)createSubButton;

@end

@protocol XDSegmentedViewDelegate<NSObject>
@required
- (NSString *)refreshBadge:(NSNumber *)index;
@end
