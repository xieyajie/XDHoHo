//
//  XDInputView.h
//  XDHoHo
//
//  Created by xieyajie on 20-11-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "XDUI.h"
//#import "XDFaceKeyboard.h"

typedef enum {
    XDInputViewStyleDefault,
    XDInputViewStyleBottom
} XDInputViewStyle;

@protocol XDInputViewDelegate;

@interface XDInputView : UIView </*XDFaceKeyboardDelegate,*/ UITextViewDelegate>
{
    UIView *_leftOuterView;
    UIView *_rightOuterView;
    UIView *_leftInnerView;
    UIView *_rightInnerView;
    
    UIImage *_backgroundImage;
    UIImage *_textBackgroundImage;
}

@property (nonatomic, weak)             id <XDInputViewDelegate>  delegate;

@property (nonatomic, strong)           NSDictionary                *userInfo;
    
@property (nonatomic)                   XDInputViewStyle          style;

@property (nonatomic, strong)           UIView                      *leftOuterView;
@property (nonatomic, strong)           UIView                      *rightOuterView;
@property (nonatomic, strong)           UIView                      *leftInnerView;
@property (nonatomic, strong)           UIView                      *rightInnerView;
@property (nonatomic, strong, readonly) UIView                      *centerView;
@property (nonatomic, strong, readonly) UIImageView                 *textBGImageView;

//@property (nonatomic, strong)           XDButton                  *faceButton;            //表情按钮
//@property (nonatomic, strong)           XDFaceKeyboard            *faceKeyboard;          //表情键盘

@property (nonatomic, strong)           UITextView                  *textView;
@property (nonatomic, strong)           NSString                    *text;
@property (nonatomic, strong)           NSString                    *placeholder;

@property (nonatomic, strong)           UIImage                     *backgroundImage;
@property (nonatomic, strong)           UIImage                     *textBackgroundImage;

@property (nonatomic)                   BOOL                        enableCustomKeyboard;
@property (nonatomic)                   BOOL                        enableFaceKeyboard;

@property (nonatomic)                   BOOL                        hideWhenResign;         //收起时隐藏到屏幕外，默认为NO
@property (nonatomic)                   BOOL                        resignWhenReturn;       //当点击键盘Return按钮时自动收起键盘，默认为YES

- (id)initWithSuperView:(UIView *)superView;                            //superView父视图
- (id)initWithSuperView:(UIView *)superView topView:(UIView *)topView;  //superView父视图。topView顶部兄弟视图，随输入工具的位置变化动态改变Frame。

- (void)refresh;

- (void)showCancelButton:(BOOL)showCancelButton;                        //显示或隐藏取消按钮

@end



@protocol XDInputViewDelegate <NSObject>

@optional

- (BOOL)inputViewShouldBeginEditing:(XDInputView *)inputView;
- (BOOL)inputViewShouldEndEditing:(XDInputView *)inputView;

- (void)inputViewDidBeginEditing:(XDInputView *)inputView;
- (void)inputViewDidEndEditing:(XDInputView *)inputView;

- (BOOL)inputView:(XDInputView *)inputView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
- (void)inputViewDidChange:(XDInputView *)inputView;

- (void)inputViewDidChangeSelection:(XDInputView *)inputView;

- (void)inputViewCancelButtonClicked:(XDInputView *)inputView;
- (void)inputViewReturnButtonClicked:(XDInputView *)inputView;

- (void)inputViewDidChangeFrame:(XDInputView *)inputView;

- (void)inputView:(XDInputView *)inputView didInputMediaWithInfo:(NSDictionary *)info;

//- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar;                   // called when bookmark button pressed
//- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar NS_AVAILABLE_IOS(3_2); // called when search results button pressed
//- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope NS_AVAILABLE_IOS(3_0);

//- (BOOL)textFieldShouldClear:(UITextField *)textField;               // called when clear button pressed. return NO to ignore (no notifications)

@end
