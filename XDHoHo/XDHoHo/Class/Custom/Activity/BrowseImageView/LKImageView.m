//
//  LKImageView.m
//  Travel
//
//  Created by tixa tixa on 13-4-23.
//  Copyright (c) 2013年 tixa. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "LKImageView.h"

#import "UIImage+Category.h"

@interface LKImageView ()
{
    CGRect  oldFrame;
    CGRect  adaptionFrame; //图片自适应大小
    
     UIImageView *_borderImageView;//边框视图
}
@end

@implementation LKImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.clipsToBounds          = YES;
        self.userInteractionEnabled = YES;
        self.contentMode            = UIViewContentModeScaleAspectFit;
    }
    return self;
}

- (void)setShowBorder:(BOOL)showBorder
{
    if (showBorder) {
        self.layer.borderWidth = 2;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
    }
}

//touch事件
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint firstPoint = [touch locationInView:touch.window];
    CGPoint selfPoint = [touch locationInView:self];
    int offX = selfPoint.x - self.frame.size.width/2.0;
    int offY = selfPoint.y - self.frame.size.height/2.0;
    
    _touchPoint = CGPointMake(firstPoint.x - offX, firstPoint.y - offY);
    
    if ([_parent respondsToSelector:_action]) {
        [_parent performSelector:_action withObject:self];
    }
}

////缩略图
//- (void)setImage:(UIImage *)image
//{
//    super.image = image;
//    
//    float appendix_x = 0;
//    float appendix_y = 0;
//    CGSize size = CGSizeZero;
//    if (_thumbMode) { //缩略图模式
//        size       = [image sizeOfScaleToFit:self.frame.size];
//        appendix_y = self.frame.size.height/2 - size.height/2;
//    } else { //全图模式
//        size       = [image sizeOfScaleToFit:self.frame.size];
//        appendix_y = self.frame.size.height/2 - size.height/2;
//        appendix_x = self.frame.size.width/2 - size.width/2;
//    }
//    oldFrame = CGRectMake(appendix_x, appendix_y, size.width, size.height);
//}


//边框
- (void)setBorderImage:(UIImage *)borderImage
{
    _borderImage = borderImage;
    if (!_borderImageView) {
        _borderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _borderImageView.image = [_borderImage resizableImageWithCompatibleCapInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0) resizingMode:UIImageResizingModeStretch];
        _borderImageView.userInteractionEnabled = YES;
        [self addSubview:_borderImageView];
    }
}

- (id)copy
{
    LKImageView *imageView = [[LKImageView alloc] init];
    imageView.frame = self.frame;
    imageView.image = self.image;
    imageView.clipsToBounds = self.clipsToBounds;
    imageView.userInteractionEnabled = self.userInteractionEnabled;
    imageView.contentMode = self.contentMode;
    imageView.originalImage = self.originalImage;
    imageView.originalURL = self.originalURL;
    imageView.normalImage = self.normalImage;
    imageView.normalURL = self.normalURL;
    return imageView;
}


@end
