//
//  XDActivityView.m
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "XDActivityView.h"

#import "RCLabel.h"
#import "XDAppendixView.h"

#import "NSString+Category.h"
#import "NSDate+Category.h"

#import "XDImageDetailView.h"

#define FOLD_TEXT_LENGTH 200
#define SHOW_LIST_MAXCOUNT 3

@interface XDActivityView () <RTLabelDelegate>

@property (nonatomic, strong) UIButton *commentButton;
@property (nonatomic, strong) UIButton *praiseButton;
@property (nonatomic, strong) UIButton *trampleButton;
@property (nonatomic, strong) UIButton *favoriteButton;
@property (nonatomic, strong) UIButton *shareButton;

@end

@implementation XDActivityView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame activity:(XDActivityModel *)activity
{
    self = [self initWithFrame:frame];
    if (self) {
        _activity = activity;
        _commentModel = nil;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame comment:(XDCommentModel *)commentModel
{
    self = [self initWithFrame:frame];
    if (self) {
        _activity = nil;
        _commentModel = commentModel;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

#pragma mark - getting

- (UIButton *)commentButton
{
    if (_commentButton == nil) {
        _commentButton = [[UIButton alloc] init];
        [_commentButton addTarget:self action:@selector(clickCommentButton:) forControlEvents:UIControlEventTouchUpInside];
//        [_commentButton setImage:[UIImage imageNamed:@"audit_icon_audit.png"] forState:UIControlStateNormal];
        _commentButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_commentButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//        _commentButton.backgroundColor = [UIColor redColor];
    }
    
    return _commentButton;
}

- (UIButton *)praiseButton
{
    if (_praiseButton == nil) {
        _praiseButton = [[UIButton alloc] init];
        [_praiseButton setTitle:@"赞" forState:UIControlStateNormal];
        [_praiseButton addTarget:self action:@selector(clickPraiseButton:) forControlEvents:UIControlEventTouchUpInside];
//        [_praiseButton setImage:[UIImage imageNamed:@"audit_icon_audit.png"] forState:UIControlStateNormal];
        _praiseButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_praiseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _praiseButton.backgroundColor = [UIColor redColor];
    }
    
    return _praiseButton;
}

- (UIButton *)trampleButton
{
    if (_trampleButton == nil) {
        _trampleButton = [[UIButton alloc] init];
        [_trampleButton setTitle:@"踩" forState:UIControlStateNormal];
        [_trampleButton addTarget:self action:@selector(clickTrampleButton:) forControlEvents:UIControlEventTouchUpInside];
//        [_trampleButton setImage:[UIImage imageNamed:@"audit_icon_audit.png"] forState:UIControlStateNormal];
        _trampleButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_trampleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _trampleButton.backgroundColor = [UIColor redColor];
    }
    
    return _trampleButton;
}

- (UIButton *)favoriteButton
{
    if (_favoriteButton == nil) {
        _favoriteButton = [[UIButton alloc] init];
        [_favoriteButton setTitle:@"收藏" forState:UIControlStateNormal];
        [_favoriteButton addTarget:self action:@selector(clickFavoriteButton:) forControlEvents:UIControlEventTouchUpInside];
//        [_favoriteButton setImage:[UIImage imageNamed:@"audit_icon_audit.png"] forState:UIControlStateNormal];
        _favoriteButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_favoriteButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _favoriteButton.backgroundColor = [UIColor redColor];
    }
    
    return _favoriteButton;
}

- (UIButton *)shareButton
{
    if (_shareButton == nil) {
        _shareButton = [[UIButton alloc] init];
        [_shareButton setTitle:@"分享" forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(clickShareButton:) forControlEvents:UIControlEventTouchUpInside];
        [_shareButton setImage:[UIImage imageNamed:@"audit_icon_audit.png"] forState:UIControlStateNormal];
        _shareButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_shareButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _shareButton.backgroundColor = [UIColor redColor];
    }
    
    return _shareButton;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - RTLabel Delegate

- (void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSString*)url
{
    
}


#pragma mark - private

#pragma mark - tool

- (NSString *)cutString:(NSString *)string toLength:(NSInteger)length
{
    if (string == nil) {
        return @"";
    }
    
    if (string.length <= length) {
        return string;
    }
    else{
        return [NSString stringWithFormat:@"%@...", [string substringToIndex:length]];
    }
}

#pragma mark - headImage
/*头像视图*/
- (UIImageView *)createHeadImageViewWithFrame:(CGRect)frame
                                       headPath:(NSString *)headPath
                                         gender:(XDAccountGender)gender
                                      accountID:(NSUInteger)accountID
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.layer.borderColor = [[UIColor colorWithWhite:0.6 alpha:0.4] CGColor];
    imageView.layer.borderWidth = 1.0;
    //测试数据
    //????
    imageView.image = [UIImage imageNamed:headPath];
    
    return imageView;
}

/*数据源头像*/
- (UIImageView *)createHeadImageViewWithFrame:(CGRect)frame
{
    if (_activity) {
        return [self createHeadImageViewWithFrame:frame headPath:_activity.headImagePath gender:_activity.gender accountID:_activity.accountId.integerValue];
    }
    
    return nil;
}

#pragma mark - name, title, content

/*通用标签高度*/
- (CGFloat)labelHeightWithFrame:(CGRect)frame text:(NSString *)text font:(UIFont *)font multiLine:(BOOL)multiLine
{
    if (multiLine) {
        CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width, 9999.0) lineBreakMode:NSLineBreakByCharWrapping];
        return size.height;
    }
    else {
        return frame.size.height;
    }
    return 0.0;
}

/*通用标签*/
- (UILabel *)createLabelWithFrame:(CGRect)frame text:(NSString *)text font:(UIFont *)font textColor:(UIColor *)textColor multiLine:(BOOL)multiLine
{
    UILabel *textLabel = [[UILabel alloc] initWithFrame:frame];
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.highlightedTextColor = [UIColor whiteColor];
    textLabel.textColor = textColor;
    textLabel.font = font;
    textLabel.text = text;
    if (multiLine) {
        textLabel.numberOfLines = 0;
        textLabel.lineBreakMode = NSLineBreakByCharWrapping;
        
        CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width, 9999.0) lineBreakMode:NSLineBreakByCharWrapping];
        frame.size.height = size.height;
        textLabel.frame = frame;
    }
    return textLabel;
}

/*姓名标签*/
- (UILabel *)createNameLabelWithFrame:(CGRect)frame text:(NSString *)text
{
    return [self createLabelWithFrame:frame
                                 text:text
                                 font:[UIFont boldSystemFontOfSize:17.0]
                            textColor:[UIColor blackColor]
                            multiLine:NO];
}

/*数据源姓名标签*/
- (UILabel *)createNameLabelWithFrame:(CGRect)frame
{
    if (_activity)
    {
        return [self createNameLabelWithFrame:frame text:_activity.name];
    }
    
    return nil;
}

#pragma mark - Appendix View

/*附件视图高度*/
- (CGFloat)appendixViewHeightWithFrame:(CGRect)frame appendixs:(NSArray *)appendixArray appendixType:(XDMediaType)appendixType thumbnailMode:(BOOL)thumbnailMode
{
    switch (appendixType) {
        case XDMediaTypeImage: {
            CGFloat l = (frame.size.width - 10.0) / 3.0;
            if (appendixArray.count > 6) {
                return l * 3 + 5.0 * 2;
            } else if (appendixArray.count > 3) {
                return l * 2 + 5.0;
            } else if (appendixArray.count > 1) {
                return l;
            } else {
                return thumbnailMode ? THUMB_IMAGE_SIZE.height : BIG_IMAGE_SIZE.height;
            }
            break;
        }
            
        case XDMediaTypeGIF: case XDMediaTypeVideo:
            return thumbnailMode ? THUMB_IMAGE_SIZE.height : BIG_IMAGE_SIZE.height;
            break;
            
        case XDMediaTypeAudio:
            return AUDIO_IMAGE_SIZE.height * appendixArray.count;
            break;
            
        case XDMediaTypeMood:
            return MOOD_IMAGE_SIZE.height;
            break;
            
        default:
            return 0.0;
            break;
    }
    
    return 0;
}

- (UIView *)createImageAppendixViewWithFrame:(CGRect)frame
                                    appendix:(NSArray *)appendixArray
                               thumbnailMode:(BOOL)thumbnailMode
{
    CGRect mFrame = frame;
    mFrame.size.height = [self appendixViewHeightWithFrame:frame appendixs:appendixArray appendixType:XDMediaTypeImage thumbnailMode:thumbnailMode];
    XDAppendixView *imagesView = [[XDAppendixView alloc] initWithFrame:mFrame
                                                              medias:appendixArray
                                                      layoutStyle:XDMediaViewLayoutStyleScaleToFill];
//    imagesView.backgroundColor = [UIColor yellowColor];
    return imagesView;
}

- (UIView *)createVideoAppendixViewWithFrame:(CGRect)frame
                                    appendix:(NSArray *)appendixArray
                               thumbnailMode:(BOOL)thumbnailMode
{
    return nil;
}

- (UIView *)createAudioAppendixViewWithFrame:(CGRect)frame
                                    appendix:(NSArray *)appendixArray
                               thumbnailMode:(BOOL)thumbnailMode
{
    UIView *audioView = [[UIView alloc] initWithFrame:frame];
    CGFloat originX = 0;
    CGFloat originY = 0;
    for (XDAppendixModel *model in appendixArray) {
        XDImageDetailView *appendixView = [[XDImageDetailView alloc] initWithFrame:CGRectMake(originX, originY, AUDIO_IMAGE_SIZE.width, AUDIO_IMAGE_SIZE.height)];
        appendixView.isAdaption = YES; //需要自适应
        appendixView.thumbMode = thumbnailMode;
        appendixView.audioPath = model.sourcePath;
        appendixView.mediaDuration = model.duration;
        appendixView.mediaType = XDMediaTypeAudio;
        
        [audioView addSubview:appendixView];
        originY += AUDIO_IMAGE_SIZE.height;
    }
    
    audioView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, originY);
    return audioView;
}

- (UIView *)createAppendixViewWithFrame:(CGRect)frame
                               appendix:(NSArray *)appendixArray
                           appendixType:(XDMediaType)appendixType
                          thumbnailMode:(BOOL)thumbnailMode
{
    switch (appendixType) {
        case XDMediaTypeImage:
            return [self createImageAppendixViewWithFrame:frame appendix:appendixArray thumbnailMode:thumbnailMode];
            break;
        case XDMediaTypeVideo:
            return [self createVideoAppendixViewWithFrame:frame appendix:appendixArray thumbnailMode:thumbnailMode];
            break;
        case XDMediaTypeAudio:
            return [self createAudioAppendixViewWithFrame:frame appendix:appendixArray thumbnailMode:thumbnailMode];
            break;
            
        default:
            return nil;
            break;
    }
}

/*附件视图*/
- (UIView *)createAppendixViewWithFrame:(CGRect)frame
                           appendix:(NSDictionary *)appendixDictionary
                          thumbnailMode:(BOOL)thumbnailMode
{
    UIView *appendixView = [[UIView alloc] initWithFrame:frame];
//    appendixView.backgroundColor = [UIColor redColor];
    
    CGFloat y = 0;
    UIView *tmpView = nil;
    for (NSString *key in appendixDictionary) {
        NSArray *array = [appendixDictionary objectForKey:key];
        tmpView = [self createAppendixViewWithFrame:CGRectMake(0, y, frame.size.width, 0.0) appendix:array appendixType:[key integerValue] thumbnailMode:thumbnailMode];
        if (tmpView) {
            [appendixView addSubview:tmpView];
            y = tmpView.frame.origin.y + tmpView.frame.size.height + 5;
        }
    }
    
    appendixView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, y + 5);
    return appendixView;
}

#pragma mark - Rich View

/*富文本视图高度*/
- (CGFloat)richViewHeightWithFrame:(CGRect)frame text:(NSString *)text
{
    if (text && text.length > 0) {
        RCLabel *richLabel = [[RCLabel alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width, frame.size.height)];
        richLabel.lineBreakMode = RTTextLineBreakModeCharWrapping;
        richLabel.componentsAndPlainText = [RCLabel extractTextStyle:text];
        
        return richLabel.optimumSize.height + 5.0;
    }
    else{
        return 0.0;
    }
}

/*富文本视图*/
- (UIView *)createActivityRichViewWithFrame:(CGRect)frame
                                 content:(NSString *)content
                           thumbnailMode:(BOOL)thumbnailMode
{
    RCLabel *richLabel = [[RCLabel alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width, frame.size.height)];
    richLabel.delegate = self;
    richLabel.lineBreakMode = RTTextLineBreakModeCharWrapping;
    
    NSMutableString *htmlString = [NSMutableString string];
    if (content && content.length > 0) {
        NSMutableString *str = [NSMutableString string];
        if (thumbnailMode)
        {
            [str appendString:[self cutString:content toLength:FOLD_TEXT_LENGTH]];
        }
        else{
            [str appendString:content];
        }
        
        
        [htmlString appendFormat:@"<font size=14>%@</font>", str];
    }
    
    if (htmlString.length > 0) {
        richLabel.componentsAndPlainText = [RCLabel extractTextStyle:htmlString];
        
        CGSize size = richLabel.optimumSize;
        CGRect rFrame = CGRectZero;
        rFrame.origin.x = frame.origin.x;//偏移量微调
        rFrame.origin.y = frame.origin.y;
        rFrame.size.width = frame.size.width;
        rFrame.size.height = size.height + 5.0;
        richLabel.frame = rFrame;
        
        return richLabel;
    }
    
    return nil;
}

#pragma mark - view setup

- (CGFloat)activityViewHeightWithShowHead:(BOOL)showHead
                                 showName:(BOOL)showName
                                 showInfo:(BOOL)showInfo
                             showAppendix:(BOOL)showAppendix
                              showAddress:(BOOL)showAddress
                           showTimeSource:(BOOL)showTimeSource
                       showOperateButtons:(BOOL)showOperateButtons
                            thumbnailMode:(BOOL)thumbnailMode
{
    CGFloat cX = 10.0;
    CGFloat cY = 10.0;
    CGFloat cW = self.frame.size.width - 20.0;
    
    //发送者姓名
    NSLog(@"Height: name-b--%f", cY);
    if (showName) {
        cY += 20.0;
    }
    
    //时间、来源
    NSLog(@"Height: date-b--%f", cY);
    if (showTimeSource) {
        cY += 20.0;
    }
    
    //正文
    NSLog(@"Height: info-b--%f", cY);
    if (showInfo) {
        cY += 10;
        NSMutableString *htmlString = [NSMutableString string];
        if (_activity.title && _activity.title.length > 0) {
            [htmlString appendFormat:@"<font size=14 color=gray>%@</font>", _activity.title];
        }
    
        if (_activity.content && _activity.content.length > 0) {
            NSString *contentStr = thumbnailMode == YES ? [self cutString:_activity.content toLength:FOLD_TEXT_LENGTH] : _activity.content;
            [htmlString appendFormat:@"<font size=14>%@</font>", contentStr];
        }
        
        CGFloat contentHeight = [self richViewHeightWithFrame:CGRectMake(cX, cY + 10.0, cW, 0.0)
                                                         text:htmlString];
        cY += contentHeight;
    }
    
    //附件视图
    NSLog(@"Height: appendix-b--%f", cY);
    if (showAppendix && _activity.isHaveAppendix) {
        cY += 10;
        for (NSString *key in _activity.appendixDictionary) {
            NSArray *array = [_activity.appendixDictionary objectForKey:key];
            CGFloat appendixHeight = [self appendixViewHeightWithFrame:CGRectMake(cX, cY, cW, 0.0) appendixs:array appendixType:[key integerValue] thumbnailMode:thumbnailMode];
            
            cY += appendixHeight + 5;
        }
    }
    
    //位置
    NSLog(@"Height: address-b--%f", cY);
    if (showAddress) {
        cY += 5;
        if (thumbnailMode) {
            NSString *address = _activity.address.length ? _activity.address : @"定位失败";
            UIFont *font = [UIFont systemFontOfSize:11.0];
            CGSize size = [address sizeWithFont:font constrainedToSize:CGSizeMake(cW, 40.0)];
            cY = cY + size.height;
        } else {
            cY = cY + 105.0;
        }
    }
    
    //操作按钮
    NSLog(@"Height: button-b--%f", cY);
    if (showOperateButtons) {
        cY += 30.0;
    }
    
    NSLog(@"Height: end--%f", cY + 10);
    //修正高度
    return cY + 10.0;
}

/*动态布局 7以下*/
- (void)setupActivityViewWithShowHead:(BOOL)showHead
                             showName:(BOOL)showName
                             showInfo:(BOOL)showInfo
                         showAppendix:(BOOL)showAppendix
                          showAddress:(BOOL)showAddress
                         addressStyle:(XDAddressStyle)addressStyle
                       showTimeSource:(BOOL)showTimeSource
                   showOperateButtons:(BOOL)showOperateButtons
                        thumbnailMode:(BOOL)thumbnailMode
{
    CGFloat cX = 10.0;
    CGFloat cY = 10.0;
    CGFloat cW = self.frame.size.width - 20.0;
    
    //发送者头像
    if (showHead) {
        UIImageView *headImageView = [self createHeadImageViewWithFrame:CGRectMake(cX, cY, 40.0, 40.0)];
        headImageView.layer.cornerRadius = 5.0;
        [self addSubview:headImageView];
        
        cW = self.frame.size.width - cX - 10;
        if (showName || showTimeSource) {
            cX = headImageView.frame.origin.x + headImageView.frame.size.width + 5.0;
        }
    }
    
    //发送者姓名
    NSLog(@"view: name-b--%f", cY);
    if (showName) {
        UILabel *nameLabel = [self createNameLabelWithFrame:CGRectMake(cX, cY, 100.0, 20.0)];
        nameLabel.font = [UIFont systemFontOfSize:16.0];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textColor = [UIColor blueColor];
        [self addSubview:nameLabel];
        
        self.commentButton.frame = CGRectMake(self.frame.size.width - 70.0, cY, 60.0, 40.0);
        [self.commentButton setTitle:[NSString stringWithFormat:@"评论 %d", _activity.commentCount] forState:UIControlStateNormal];
        [self addSubview:self.commentButton];
        
        cY = nameLabel.frame.origin.y + nameLabel.frame.size.height;
        if (!showTimeSource) {
            cX = 10.0;
        }
    }
    
    //时间、来源
    NSLog(@"view: date-b--%f", cY);
    if (showTimeSource) {
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(cX, cY + 5.0, 100.0, 15)];
        dateLabel.font = [UIFont systemFontOfSize:13.0];
        dateLabel.textColor = [UIColor grayColor];
        dateLabel.backgroundColor = [UIColor clearColor];
        dateLabel.text = _activity.createDate.timeIntervalDescription;
        [self addSubview:dateLabel];
        
        cY = dateLabel.frame.origin.y + dateLabel.frame.size.height;
        cX = 10.0;
    }
    
    //正文
    NSLog(@"view: info-b--%f", cY);
    if (showInfo) {
        UIView *contentView = [self createActivityRichViewWithFrame:CGRectMake(cX, cY + 10.0, cW, 0.0) content:_activity.content thumbnailMode:thumbnailMode];
        if (contentView != nil) {
            contentView.backgroundColor = [UIColor clearColor];
            [self addSubview:contentView];
            cY = contentView.frame.origin.y + contentView.frame.size.height;
        }
    }
    
    //附件视图
    NSLog(@"view: appendix-b--%f", cY);
    if (showAppendix && _activity.isHaveAppendix) {
        UIView *appendixView = [self createAppendixViewWithFrame:CGRectMake(cX, cY + 5.0, cW, 0.0)
                                                        appendix:_activity.appendixDictionary
                                                   thumbnailMode:YES];
        [self addSubview:appendixView];
//        appendixView.backgroundColor = [UIColor greenColor];
        cY = appendixView.frame.origin.y + appendixView.frame.size.height;
    }
    
    //位置
    NSLog(@"view: address-b--%f", cY);
    if (showAddress) {
        UIView *addressView = nil;
        if (addressStyle == XDAddressStyleText) {
            NSString *address = _activity.address.length ? _activity.address : @"定位失败";
            UIFont *font = [UIFont systemFontOfSize:11.0];
            CGSize size = [address sizeWithFont:font constrainedToSize:CGSizeMake(cW, 40.0)];
            
            addressView = [[UIView alloc] initWithFrame:CGRectMake(cX, cY + 5.0, cW, size.height)];
            addressView.backgroundColor = [UIColor clearColor];
            UIImageView *addressIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, addressView.frame.size.height)];
            addressIcon.contentMode = UIViewContentModeCenter;
            addressIcon.image = [UIImage imageNamed:@"address.png"];
            [addressView addSubview:addressIcon];
            UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(addressIcon.frame.origin.x + addressIcon.frame.size.width, 0, addressView.frame.size.width - (addressIcon.frame.origin.x + addressIcon.frame.size.width), addressView.frame.size.height)];
            addressLabel.numberOfLines = 0;
            addressLabel.backgroundColor = [UIColor clearColor];
            addressLabel.font = font;
            addressLabel.textColor = [UIColor grayColor];
            addressLabel.text = address;
            [addressView addSubview:addressLabel];
            [self addSubview:addressView];
        }
        else if (addressStyle == XDAddressStyleMap)
        {
            
        }
        
        cY = addressView.frame.origin.y + addressView.frame.size.height;
    }
    
    //操作按钮：评论，赞，踩，收藏，分享
    NSLog(@"view: button-b--%f", cY);
    if (showOperateButtons) {
        self.shareButton.frame = CGRectMake(self.frame.size.width - 50.0, cY + 5.0, 40.0, 25.0);
        [self addSubview:self.shareButton];
        
        self.praiseButton.frame = CGRectMake(cX, cY + 5.0, 40.0, 25.0);
        [self addSubview:self.praiseButton];
        
        self.trampleButton.frame = CGRectMake(self.praiseButton.frame.origin.x + self.praiseButton.frame.size.width + 15, cY + 5.0, 40.0, 25.0);
        [self addSubview:self.trampleButton];
        
        self.favoriteButton.frame = CGRectMake(self.trampleButton.frame.origin.x + self.trampleButton.frame.size.width + 15, cY + 5.0, 40.0, 25.0);
        [self addSubview:self.favoriteButton];
        
        cY += 30.0;
    }
    else{
        _shareButton = nil;
        _praiseButton = nil;
        _trampleButton = nil;
        _favoriteButton = nil;
    }
    
    //修正高度
    cY += 10.0;
    NSLog(@"view: end--%f", cY);
    
    CGRect frame = self.frame;
    frame.size.height = cY;
    self.frame = frame;
}

/*动态布局 7*/
- (void)setupActivityViewAbove7WithShowHead:(BOOL)showHead
                             showName:(BOOL)showName
                             showInfo:(BOOL)showInfo
                          showAddress:(BOOL)showAddress
                       showTimeSource:(BOOL)showTimeSource
                   showOperateButtons:(BOOL)showOperateButtons
                        thumbnailMode:(BOOL)thumbnailMode
{
    //UIText Kit
    //????
}

#pragma mark - public

- (CGFloat)activityViewHeight;
{
    return [self activityViewHeightWithShowHead:YES showName:YES showInfo:YES showAppendix:YES showAddress:NO showTimeSource:YES showOperateButtons:YES thumbnailMode:NO];
}

- (void)setupActivityView
{
    BOOL showHead = NO;
    NSArray *array = [_activity.appendixDictionary objectForKey:[NSNumber numberWithInteger:XDMediaTypeAudio]];
    if (array && [array count] > 0) {
        showHead = YES;
    }
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
//        [self setupActivityViewAbove7WithShowHead:YES showName:YES showInfo:YES showAddress:YES showTimeSource:YES showAuditButton:YES showAuditCount:NO showAudits:YES showCommentButton:YES showCommentCount:NO showComments:YES thumbnailMode:YES];
        
        [self setupActivityViewWithShowHead:showHead showName:YES showInfo:YES showAppendix:YES showAddress:NO addressStyle:0 showTimeSource:YES showOperateButtons:YES thumbnailMode:NO];
    }
    else{
        [self setupActivityViewWithShowHead:showHead showName:YES showInfo:YES showAppendix:YES showAddress:NO addressStyle:0 showTimeSource:YES showOperateButtons:YES thumbnailMode:NO];
    }
}

- (CGFloat)commentViewHeight
{
    CGFloat cY = 10 + 20 + 20 + 5;
    CGFloat contentHeight = [self richViewHeightWithFrame:CGRectMake(10, cY + 10.0, self.frame.size.width - 20.0 - 20, 0.0) text:_commentModel.content];
    return cY + contentHeight + 10;
}

- (void)setupCommentView
{
    CGFloat cX = 10.0;
    CGFloat cY = 10.0;
    CGFloat cW = self.frame.size.width - 20.0;
    
    //发送者头像
//    UIImageView *headImageView = [self createHeadImageViewWithFrame:CGRectMake(cX, cY, 40.0, 40.0)];
//    headImageView.layer.cornerRadius = 5.0;
//    [self addSubview:headImageView];
//    cX = headImageView.frame.origin.x + headImageView.frame.size.width + 8.0;
//    cW = self.frame.size.width - cX - 12.0;
    
    //发送者姓名
    UILabel *nameLabel = [self createNameLabelWithFrame:CGRectMake(cX, cY, cW - 60.0, 20.0) text:_commentModel.name];
    nameLabel.font = [UIFont systemFontOfSize:13.0];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textColor = [UIColor blueColor];
    [self addSubview:nameLabel];
    cY = nameLabel.frame.origin.y + nameLabel.frame.size.height;
    
    //时间
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(cX, cY + 5.0, 100.0, 15)];
    dateLabel.font = [UIFont systemFontOfSize:12.0];
    dateLabel.textColor = [UIColor grayColor];
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.text = _commentModel.createDate.timeIntervalDescription;
    [self addSubview:dateLabel];
    
    cY = dateLabel.frame.origin.y + dateLabel.frame.size.height;
    
    //正文
    UIView *contentView = [self createActivityRichViewWithFrame:CGRectMake(cX, cY + 5.0, cW, 0.0) content:_commentModel.content thumbnailMode:NO];
    contentView.tag = -1;
    [self addSubview:contentView];
    cY = contentView.frame.origin.y + contentView.frame.size.height;
    
    //修正高度
    cY += 10.0;
    
    CGRect frame = self.frame;
    frame.size.height = cY;
    self.frame = frame;
}

#pragma mark - Action

/*点击评论按钮*/
- (void)clickCommentButton:(id)sender
{
//    if ([_delegate respondsToSelector:@selector(contactActivityViewCommentButtonClicked:)]) {
//        [_delegate contactActivityViewCommentButtonClicked:self];
//    }
}

/*点击赞按钮*/
- (void)clickPraiseButton:(id)sender
{
    
}

/*点击踩按钮*/
- (void)clickTrampleButton:(id)sender
{
    
}

/*点击收藏按钮*/
- (void)clickFavoriteButton:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(contactActivityViewFavoriteButtonClicked:)]) {
        [_delegate contactActivityViewFavoriteButtonClicked:self];
    }
}

/*点击分享按钮*/
- (void)clickShareButton:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(contactActivityViewShareButtonClicked:)]) {
        [_delegate contactActivityViewShareButtonClicked:self];
    }
}

@end
