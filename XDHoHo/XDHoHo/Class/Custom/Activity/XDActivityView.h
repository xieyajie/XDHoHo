//
//  XDActivityView.h
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XDActivityModel.h"
#import "XDCommentModel.h"

@protocol XDActivityViewDelegate;

@interface XDActivityView : UIView

@property (nonatomic, weak) id<XDActivityViewDelegate> delegate;

@property (nonatomic, strong) XDActivityModel *activity;     //动态数据源
@property (nonatomic, strong) XDCommentModel *commentModel;  //评论数据源
@property (nonatomic, strong) NSDictionary *otherInfo;       //其他信息

- (id)initWithFrame:(CGRect)frame activity:(XDActivityModel *)activity;
- (id)initWithFrame:(CGRect)frame comment:(XDCommentModel *)commentModel;

- (CGFloat)activityViewHeight;
- (void)setupActivityView;

- (CGFloat)commentViewHeight;
- (void)setupCommentView;

@end

@protocol XDActivityViewDelegate <NSObject>

- (void)contactActivityViewShareButtonClicked:(XDActivityView *)activityView; //点击分享按钮
- (void)contactActivityViewPraiseButtonClicked:(XDActivityView *)activityView; //点击赞按钮
- (void)contactActivityViewTrampleButtonClicked:(XDActivityView *)activityView; //点击踩按钮
- (void)contactActivityViewFavoriteButtonClicked:(XDActivityView *)activityView; //点击收藏按钮

@end
