//
//  XDAppendixView.m
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//


#import "XDAppendixView.h"

#import "LKImageView.h"
#import "LKImageManager.h"
#import "LKScrollView.h"

#import "NSString+Category.h"

#define SEPARATOR 5.0

@interface XDAppendixView ()
{
    UIView *_contentView;
    
    LKImageManager *_imageManager;
}
@end

@implementation XDAppendixView

- (id)initWithFrame:(CGRect)frame medias:(NSArray *)medias layoutStyle:(XDMediaViewLayoutStyle)layoutStyle
{
    self = [super initWithFrame:frame];
    if (self) {
        _medias = medias;
        _layoutStyle = layoutStyle;
        
        CGRect mFrame = frame;
        mFrame.size.height = [self heightWithFrame:frame medias:medias layoutStyle:layoutStyle];
        self.frame = mFrame;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame media:(XDAppendixModel *)media layoutStyle:(XDMediaViewLayoutStyle)layoutStyle
{
    return [self initWithFrame:frame
                        medias:media ? @[media] : nil
                   layoutStyle:layoutStyle];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:_contentView];
        
        CGFloat l = (_contentView.frame.size.width - SEPARATOR * 2) / 3;
        for (NSInteger i = 0; i < _medias.count; i++) {
            CGFloat x = (i % 3) * (l + SEPARATOR);
            CGFloat y = (i / 3) * (l + SEPARATOR);
            
            XDAppendixModel *media = [_medias objectAtIndex:i];
            
            LKImageView *imageView = [[LKImageView alloc] initWithFrame:CGRectMake(x, y, l, l)];
            imageView.tag = i;
//            imageView.normalURL = media.mediumImagePath;
//            imageView.originalURL = media.sourcePath;
            imageView.image = [self smallImageWithMedia:media layoutStyle:XDMediaViewLayoutStyleScaleToFill target:imageView];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.parent = self;
            imageView.action = @selector(showLKImageView:);
//            imageView.backgroundColor = [UIColor clearColor];
            
            [_contentView addSubview:imageView];
        }
    }
}



#pragma mark - Height

/*多媒体视图高度*/
- (CGFloat)heightWithFrame:(CGRect)frame medias:(NSArray *)medias layoutStyle:(XDMediaViewLayoutStyle)layoutStyle
{
    if (medias.count > 1) {
        CGFloat l = (frame.size.width - SEPARATOR * 2) / 3;
        if (medias.count > 6) {
            return l * 3 + SEPARATOR * 2;
        } else if (medias.count > 3) {
            return l * 2 + SEPARATOR;
        } else {
            return l;
        }
    } else if (medias.count == 1) {
        CGSize mediaSize = [self mediaDetailViewSizeWithFrame:frame media:medias.lastObject layoutStyle:layoutStyle];
        return mediaSize.height;
    }
    return 0.0;
}

/*多媒体视图高度*/
+ (CGFloat)heightWithFrame:(CGRect)frame medias:(NSArray *)medias layoutStyle:(XDMediaViewLayoutStyle)layoutStyle
{
    return [[XDAppendixView alloc] heightWithFrame:frame medias:medias layoutStyle:layoutStyle];
}

/*多媒体视图高度*/
+ (CGFloat)heightWithFrame:(CGRect)frame media:(XDAppendixModel *)media layoutStyle:(XDMediaViewLayoutStyle)layoutStyle
{
    return [self heightWithFrame:frame
                          medias:media ? @[media] : nil
                     layoutStyle:layoutStyle];
}



#pragma mark - Media Detail View

- (CGSize)mediaDetailViewSizeWithFrame:(CGRect)frame media:(XDAppendixModel *)media layoutStyle:(XDMediaViewLayoutStyle)layoutStyle
{
    CGSize mediaSize = CGSizeZero;
    switch (media.type) {
        case XDMediaTypeImage: case XDMediaTypeGIF: case XDMediaTypeVideo:
            if (layoutStyle == XDMediaViewLayoutStyleScaleToFill) {
                mediaSize = frame.size;
            } else if (layoutStyle == XDMediaViewLayoutStyleThumbnail) {
                mediaSize = THUMB_IMAGE_SIZE;
            } else if (layoutStyle == XDMediaViewLayoutStyleDetail) {
                mediaSize = BIG_IMAGE_SIZE;
            }
            break;
            
        case XDMediaTypeAudio:
            mediaSize = AUDIO_IMAGE_SIZE;
            break;
            
        case XDMediaTypeMood:
            mediaSize = MOOD_IMAGE_SIZE;
            break;
            
        default:
            break;
    }
    return mediaSize;
}

/*获取缩略图*/
- (UIImage *)smallImageWithMedia:(XDAppendixModel *)media layoutStyle:(XDMediaViewLayoutStyle)layoutStyle target:(id)target
{
    UIImage *smallImage = nil;
    if (media.type == XDMediaTypeMood) {//心情
        if (media.sourcePath.isValidImageURL) {
            //????
        } else {
            //????
        }
    }
    else if (media.type == XDMediaTypeImage || media.type == XDMediaTypeGIF || media.type == XDMediaTypeVideo) {
        if ((layoutStyle == XDMediaViewLayoutStyleDetail) && media.type == XDMediaTypeImage) {//图片大图
            smallImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:media.mediumImagePath]]];
        }
        else {
            if (media.type == XDMediaTypeGIF) {
               //????
            }
            else if ([media.smallImagePath hasPrefix:NSHomeDirectory()]) {//本地
                //????
            }
            else {
                //测试
                //????
                smallImage = [UIImage imageNamed:media.sourcePath];
            }
        }
    }
    return smallImage;
}



#pragma mark - Images Browser

/*多图浏览工具*/
- (void)showLKImageView:(LKImageView *)sender
{
    _imageManager = [[LKImageManager alloc] init];
    
    NSMutableArray *scrollViewsArray = [NSMutableArray array];
    for (NSInteger i = 0; i < _medias.count; i++) {
        XDAppendixModel *media = [_medias objectAtIndex:i];
        
        LKImageView *imageView = [[LKImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, sender.frame.size.width, sender.frame.size.height)];
        imageView.normalURL = media.mediumImagePath;
        imageView.originalURL = media.sourcePath;
        imageView.image = [self smallImageWithMedia:media layoutStyle:XDMediaViewLayoutStyleScaleToFill target:imageView];

        LKScrollView *scrollView = [LKScrollView scrollViewWithImageView:imageView];
        [scrollViewsArray addObject:scrollView];
    }
    _imageManager.corner = 0.0;
    _imageManager.column = 3;
    _imageManager.edge = 5.0;
    _imageManager.touchIndex = sender.tag;
    _imageManager.touchPoint = sender.touchPoint;
    [_imageManager showWithScrollList:scrollViewsArray];
}

@end
