//
//  XDImageDetailView.h
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ASIHTTPRequest.h"

@interface XDImageDetailView : UIButton<UIScrollViewDelegate/*,ASIHTTPRequestDelegate*/,UIActionSheetDelegate>
@property (nonatomic) BOOL isAdaption; //是否自适应图片大小   默认NO
@property (nonatomic) BOOL thumbMode; // 缩略图 还是 大图    默认YES（缩略图）
@property (nonatomic) XDMediaType mediaType; //附件类型   默认是指 XDMediaTypeImage
@property (nonatomic) BOOL showOriginal; // 创建 就显示原图  默认NO
@property (nonatomic) BOOL showBorder; // 显示边框  默认NO
@property (nonatomic) BOOL showGifTag; // 显示gif标记

@property (nonatomic, strong) UIImage *image;//小图

@property (nonatomic, strong) UIImage *normalImage;//中图
@property (nonatomic, strong) NSString *normalURL;//中图路径

@property (nonatomic, strong) UIImage *originalImage;//原图
@property (nonatomic, strong) NSString *originalURL;//原图路径

@property (nonatomic, strong) UIImage *borderImage;//边框图片

@property (nonatomic, strong) NSString *downloadFileDir;//原图下载保存目录 默认 @"Documents/ImageCache"

@property (nonatomic, strong, readonly) UIImageView *thumbnailImageView;

@property (nonatomic, strong) NSString *audioPath;//音频路径
@property (nonatomic) CGFloat mediaDuration; //多媒体长度（秒）

- (void)enterFullScreen;//进入全屏

@end
