//
//  XDInfoDetailViewController.h
//  XDHoHo
//
//  Created by xie yajie on 13-11-17.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDViewController.h"

@class XDActivityModel;
@interface XDInfoDetailViewController : XDViewController

- (id)initWithActivityModel:(XDActivityModel *)activityModel;

@end
