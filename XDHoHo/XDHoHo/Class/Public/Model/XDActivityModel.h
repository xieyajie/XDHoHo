//
//  XDActivityModel.h
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDModel.h"

@interface XDActivityModel : XDModel

@property (nonatomic) NSInteger commentCount;//评论的数目
@property (nonatomic) NSInteger praiseCount;//赞的数目
@property (nonatomic) NSInteger trampleCount;//踩的数目
@property (nonatomic) NSInteger favoriteCount;//收藏的数目

@end
