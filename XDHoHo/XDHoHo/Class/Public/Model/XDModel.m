//
//  XDModel.m
//  XDUI
//
//  Created by xie yajie on 13-10-26.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDModel.h"

@implementation XDModel

@synthesize ID, accountId, name, headImagePath, gender, createDate;
@synthesize title, subtitle, content;
@synthesize appendixDictionary;
//@synthesize isHaveAppendix, appendixType, appendixPath, appendixNormallPath, appendixThumbnailPath, appendixOriginalPath, appendixDuration;

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        //
    }
    
    return self;
}

@end
