//
//  XDModel.h
//  XDUI
//
//  Created by xie yajie on 13-10-26.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XDModel : NSObject

@property (nonatomic, copy) NSString *ID; //动态ID
@property (nonatomic, copy) NSString *accountId; //动态发布人的id
@property (nonatomic, copy) NSString *name; //动态发布人的姓名
@property (nonatomic, copy) NSString *headImagePath; //动态发布人的头像路径
@property (nonatomic) XDAccountGender gender; //动态发布人的性别
@property (nonatomic, copy) NSDate *createDate; //动态发布的时间
@property (nonatomic, copy) NSString *address; //动态发布的地址
//@property (nonatomic) CLLocationCoordinate2D coordinate; //动态发布的坐标

@property (nonatomic, strong) NSString *title; //标题
@property (nonatomic, strong) NSString *subtitle; //子标题
@property (nonatomic, copy) NSString *content; //内容

//附件
@property (nonatomic, strong) NSMutableDictionary  *appendixDictionary;  //附件列表

//判断标志
@property (nonatomic) BOOL isHaveAppendix;      //动态是否有附件
@property (nonatomic) BOOL isHaveComment;      //动态是否有评论
@property (nonatomic) BOOL isHaveAudit;      //动态是否有审批


- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
