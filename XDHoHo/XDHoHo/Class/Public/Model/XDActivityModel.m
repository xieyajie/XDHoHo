//
//  XDActivityModel.m
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDActivityModel.h"

#import "XDAppendixModel.h"

#import "NSMutableArray+Category.h"
#import "NSString+Category.h"
#import "RegexKitLite.h"

@implementation XDActivityModel

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.ID = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ID] integerValue]];
        self.accountId = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ACCOUNTID] integerValue]];
        self.name = [dictionary objectForKey:KMODEL_NAME];
        self.headImagePath = [dictionary objectForKey:KMODEL_HEADPATH];
//        self.gender = [dictionary objectForKey:KMODEL_GENDER];
        self.createDate = [dictionary objectForKey:KMODEL_CREATEDATE];
        self.address = [dictionary objectForKey:KMODEL_ADDRESS];
//        self.coordinate = CLLocationCoordinate2DMake([[dictionary objectForKey:KMODEL_LATITUDE] doubleValue], [[dictionary objectForKey:KMODEL_LONGITUDE] doubleValue]);
        
        //内容
        self.title = [dictionary objectForKey:KMODEL_TITLE];
        self.content = [dictionary objectForKey:KMODEL_CONTENT];
        
        //附件列表
        self.isHaveAppendix = NO;
        
        NSString *regexString = @"\\[心情:.+?\\]";
        NSString *matchedString = [self.content stringByMatching:regexString];
        if (matchedString.length) {//心情
            self.content = [[self.content stringByReplacingOccurrencesOfRegex:regexString withString:@""] trimmedString];
            self.isHaveAppendix = YES;
        }
        
        NSDictionary *appendixDic = [dictionary objectForKey:KAPPENDIX];
        if ([appendixDic count] != 0) {
            [self setupAppendixWithDictionary:appendixDic];
            self.isHaveAppendix = YES;
        }
        
        //数量
        NSString *commentStr = [dictionary objectForKey:KACTIVITY_COUNT_COMMENT];
        self.commentCount = (commentStr == nil || commentStr.length == 0) ? 0 : [commentStr integerValue];
        
        NSString *praiseStr = [dictionary objectForKey:KACTIVITY_COUNT_PRAISE];
        self.praiseCount = (praiseStr == nil || praiseStr.length == 0) ? 0 : [praiseStr integerValue];
        
        NSString *trampleStr = [dictionary objectForKey:KACTIVITY_COUNT_TRAMPLE];
        self.trampleCount = (trampleStr == nil || trampleStr.length == 0) ? 0 : [trampleStr integerValue];
        
        NSString *favoriteStr = [dictionary objectForKey:KACTIVITY_COUNT_FAVORITE];
        self.favoriteCount = (favoriteStr == nil || favoriteStr.length == 0) ? 0 : [favoriteStr integerValue];
    }
    
    return self;
}

- (NSArray *)appendixModelsForArray:(NSArray *)array
{
    NSMutableArray *results = [NSMutableArray array];
    
    if (array == nil || [array count] == 0) {
        return results;
    }
    
    for (NSDictionary *dic in array) {
        XDAppendixModel *appendixModel = [[XDAppendixModel alloc] initWithDictionary:dic];
        if (appendixModel != nil) {
            [results addObject:appendixModel];
        }
    }
    
    return results;
}

- (void)setupAppendixWithDictionary:(NSDictionary *)dictionary
{
    if (!self.appendixDictionary) {
        self.appendixDictionary = [[NSMutableDictionary alloc] init];
    }
    else{
        [self.appendixDictionary removeAllObjects];
    }
    
    NSArray *images = [dictionary objectForKey:KAPPENDIX_IMAGE];
    if (images && [images count] > 0) {
        [self.appendixDictionary setObject:[self appendixModelsForArray:images] forKey:[NSNumber numberWithInteger:XDMediaTypeImage]];
    }
    
    NSArray *videos = [dictionary objectForKey:KAPPENDIX_VIDEO];
    if (videos && [videos count] > 0) {
        [self.appendixDictionary setObject:[self appendixModelsForArray:videos] forKey:[NSNumber numberWithInteger:XDMediaTypeVideo]];
    }
    
    NSArray *audios = [dictionary objectForKey:KAPPENDIX_AUDIO];
    if (audios && [audios count] > 0) {
        [self.appendixDictionary setObject:[self appendixModelsForArray:audios] forKey:[NSNumber numberWithInteger:XDMediaTypeAudio]];
    }
}


@end
