//
//  XDViewController.h
//  XDUI
//
//  Created by xieyajie on 13-10-14.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XDViewController : UIViewController

@property (nonatomic) CGFloat originY;
@property (nonatomic) CGFloat sizeHeight;

@end
